<?xml version="1.0" encoding="UTF-8"?>
<tileset name="powerups" tilewidth="24" tileheight="16" tilecount="4">
 <tile id="0">
  <properties>
   <property name="cls" value="etank"/>
  </properties>
  <image width="24" height="8" source="../images/objects/powerups/etank.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="cls" value="life_orb"/>
  </properties>
  <image width="16" height="16" source="../images/objects/powerups/life_orb-0.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="cls" value="map"/>
  </properties>
  <image width="16" height="16" source="../images/objects/powerups/map-7.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="cls" value="map_disk"/>
  </properties>
  <image width="16" height="16" source="../images/objects/powerups/map_disk.png"/>
 </tile>
</tileset>
