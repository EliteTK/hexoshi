<?xml version="1.0" encoding="UTF-8"?>
<tileset name="stones" tilewidth="16" tileheight="16" tilecount="1">
 <tile id="0">
  <properties>
   <property name="cls" value="weak_stone"/>
  </properties>
  <image width="16" height="16" source="../images/objects/stones/weak_stone.png"/>
 </tile>
</tileset>
